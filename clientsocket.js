'use strict'

var net = require('net')


module.exports = function ClientSocket(arrServers, indexServers, message){

    return new Promise( (resolve, reject) => {
        var client = new net.Socket()

        client.connect(arrServers[indexServers].port, arrServers[indexServers].host, () =>{
            if (indexServers >= arrServers.length) {indexServers=0}
            client.write(message)
        })
        client.on('data', (rChunck) =>{
            resolve(rChunck)
            client.destroy()
        })
        client.on('close', () => {
            console.log('connection closed')
            client.destroy()
        })
        client.on('error', (err) => {
            console.log(err)
            reject(new Error(err))
            client.destroy()
        })
    })    
}