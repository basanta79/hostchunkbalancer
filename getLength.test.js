let mess = require('./ifsf_mess')

let complete1 = Buffer.concat([
    Buffer.from("0212110020"), 
    Buffer.from([0xC1,0x20, 0xC1, 0X80]), 
    Buffer.from("(000000000000250000041100005048986519041100012921020121212C101150855410510031377071363170010428965=2712105380251110019728752NL00001700030470331"),
    Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
    Buffer.from("NL0000000001010014104950\\97801600000020034278630011")
]) 

let complete2 = Buffer.concat([
    Buffer.from("0212110020"), 
    Buffer.from([0xC1,0x20, 0xC1, 0X80]), 
    Buffer.from("(000000000000250000041200005048986519041100012921020121212C101150855410510031377071363170010428965=2712105380251110019728752NL00001700030470331"),
    Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
    Buffer.from("NL0000000001010014104950\\97801600000020034278630011")
]) 

test('Message Complete', () => {

    mess.storeMessage(complete1)
    expect(mess.extractMessage()).toStrictEqual(complete1)
}); 

test('Message Partial', () => {

    let part1 = Buffer.concat([
        Buffer.from("0212110020"), 
        Buffer.from([0xC1,0x20, 0xC1, 0X80]), 
        Buffer.from("(0000000000002500000411000050489865190411000129210201212")
    ]) 

    let part2 = Buffer.concat([
        Buffer.from("12C101150855410510031377071363170010428965=2712105380251110019728752NL00001700030470331"),
        Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        Buffer.from("NL0000000001010014104950\\97801600000020034278630011")
    ]) 

    mess.storeMessage(part1)
    mess.storeMessage(part2)
    expect(mess.extractMessage()).toStrictEqual(complete1)
});

test('Message concat Complete', () => {

    let messageConcat = Buffer.concat([ complete1, complete2 ])

    mess.storeMessage(messageConcat)
    expect(mess.extractMessage()).toStrictEqual(complete1)
    expect(mess.extractMessage()).toStrictEqual(complete2)
});

test('Message concat Partial', () => {

    let part1 = Buffer.concat([
        Buffer.from("0212110020"), 
        Buffer.from([0xC1,0x20, 0xC1, 0X80]), 
        Buffer.from("(000000000000250000041100005048986519041100012921020121212C101150855410510031377071363170010428965=2712105380251110019728752NL00001700030470331"),
        Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        Buffer.from("NL0000000001010014104950\\97801600000020034278630011"),
        Buffer.from("0212110020"), 
        Buffer.from([0xC1,0x20, 0xC1, 0X80]), 
        Buffer.from("(0000000000002500000412000050489865190411000129210201212")
    ]) 

    let part2 = Buffer.concat([
        Buffer.from("12C101150855410510031377071363170010428965=2712105380251110019728752NL00001700030470331"),
        Buffer.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
        Buffer.from("NL0000000001010014104950\\97801600000020034278630011")
    ]) 

    mess.storeMessage(part1)
    mess.storeMessage(part2)
    expect(mess.extractMessage()).toStrictEqual(complete1)
    expect(mess.extractMessage()).toStrictEqual(complete2)
});