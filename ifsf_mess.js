'use strict'

let missatge = Buffer.from("")

function storeMessage (message) {
    missatge = Buffer.concat( [missatge, message] )
}

function extractMessage (arr) {
    let bufRet = Buffer.from("")

    let lengthExpected = parseInt(missatge.slice(0,4).toString()) + 4
    let lengthBuffer = missatge.length

    if (lengthBuffer >= lengthExpected){
        bufRet = missatge.slice(0, lengthExpected)
        missatge = missatge.slice(lengthExpected)
        arr.push(bufRet)
        extractMessage(arr)
    }
}

function cleanBuffer () {
    missatge = Buffer.from("")
}

module.exports = {
    storeMessage: storeMessage,
    extractMessage: extractMessage,
    cleanBuffer: cleanBuffer,
}