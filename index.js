'use strict'

var net = require('net')
var arrServers = require('./hostbalancer.json')
var indexServers = 0
let mess = require('./ifsf_mess')
const send = require('./clientsocket')
require('dotenv').config();

var server = net.createServer(socket => {
    let d 
    socket.on('data', (chunck) => {
        let arr = []
        mess.storeMessage(chunck)
        mess.extractMessage(arr) 
        console.log(arr.length)
        console.log({arr})
        arr.forEach( (item) => {
            send(arrServers, indexServers, item).then( (data) => {
                socket.write(data).catch(error => {
                    console.log('Este mensaje no se ha procesado debido a un error en el servidor')
                })
            })
            indexServers++
            if (indexServers >= arrServers.length) { indexServers = 0}
        })
        
    })

    socket.on('error', (error)=> {
        console.log({error})
    })
        
})

server.listen('3010',  ()=>{
    console.log(arrServers)
    console.log('Listening on port 3010')
})

server.on('connection', () => {
    console.log('connected OK')
})

server.on('end', () => {
    server.listen('3010',  ()=>{
        console.log(arrServers)
        console.log('Listening on port 3000')
    })
})
